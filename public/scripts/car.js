class Car {
  static list = [];

  static init(cars) {
    this.list = cars.map((i) => new this(i));
  }

  constructor({
    id,
    plate,
    manufacture,
    model,
    image,
    rentPerDay,
    capacity,
    description,
    transmission,
    available,
    driverType,
    type,
    year,
    options,
    specs,
    availableAt,
  }) {
    this.id = id;
    this.plate = plate;
    this.manufacture = manufacture;
    this.model = model;
    this.image = image;
    this.rentPerDay = rentPerDay;
    this.capacity = capacity;
    this.description = description;
    this.transmission = transmission;
    this.available = available;
    this.driverType = driverType;
    this.type = type;
    this.year = year;
    this.options = options;
    this.specs = specs;
    this.availableAt = availableAt;
  }

  render() {
    return `
        <div class="card" style="object-fit: cover;">
            <img
                src="${this.image}"
                class="card-img-top"
                width="330"
                height="220"
            />
            <div class="card-body">
                <p>${this.manufacture} ${this.model}/${this.type}</p>
                <h6 class="card-title"><strong>Rp ${this.rentPerDay
                  .toString()
                  .replace(
                    /\B(?<!\.\d*)(?=(\d{3})+(?!\d))/g,
                    "."
                  )} / hari </strong>${
      this.driverType == 1 ? "(Dengan Supir)" : "(Lepas Kunci)"
    }</h6>
                <p class="card-text">
                ${this.description}
                </p>
                <p>
                <span><img src="./icons/fi_users.png" /></span>&nbsp;&nbsp;${
                  this.capacity
                }
                orang
                </p>
                <p>
                <span><img src="./icons/fi_settings.png" /></span>&nbsp;&nbsp;${
                  this.transmission
                }
                </p>
                <p>
                <span><img src="./icons/fi_calendar.png" /></span>&nbsp;&nbsp;Tahun ${
                  this.year
                }
                </p>
                <button class="btn btn-green w-100" onclick="redirect(/mobil/${
                  this.id
                })">Pilih Mobil</button>
            </div>
        </div>
      `;
  }
}
