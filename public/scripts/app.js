class App {
  constructor() {
    this.driverType = document.getElementById("driverType");
    this.rentDate = document.getElementById("rentDate");
    this.rentTime = document.getElementById("rentTime");
    this.passanger = document.getElementById("passanger");
    this.searchBtn = document.getElementById("search-btn");
    this.carContainer = document.getElementById("cars-container");
  }

  async init() {
    // Register click listener
    this.searchBtn.onclick = this.run;
  }

  run = () => {
    this.load().then(() => {
      this.clear();
      Car.list.forEach((car) => {
        const node = document.createElement("div");
        node.classList.add("col-lg-4", "py-3");
        node.innerHTML = car.render();
        this.carContainer.appendChild(node);
      });
    });
  };

  async load() {
    const cars = await Binar.listCars(
      this.driverType.value &&
        this.rentDate.value &&
        this.rentTime.value &&
        this.passanger.value
        ? this.filter
        : null
    );
    console.log(cars);
    Car.init(cars);
  }

  clear = () => {
    let child = this.carContainer.firstElementChild;

    while (child) {
      child.remove();
      child = this.carContainer.firstElementChild;
    }
  };

  filter = (e) => {
    return (
      e.driverType == this.driverType.value &&
      e.capacity >= this.passanger.value &&
      e.availableAt < `${this.rentDate.value}T${this.rentTime.value}Z`
    );
  };
}
